import java.util.Scanner;

public class ArraysApp {
    public static void main(String[] args) {
        int max=100;
        int[] array1 = new int[max];
        String name;
        System.out.print("Enter name: ");
        Scanner scn=new Scanner(System.in);
        name=scn.next();
        System.out.println("Let the game begin!");
        int rnd = (int) (Math.random() * (max+1));
        for (int i = 0; i < array1.length; i++) {
            System.out.print("Enter the number: ");
            int userNum=scn.nextInt();
            if (userNum<rnd){
                System.out.println("Your number is too small. Please, try again.");
            }
            else if(userNum>rnd)
            {
                System.out.println("Your number is too big. Please, try again.");
            }
            else
                System.out.println("Congratulations " + name +"!");
        }
        for (int j = 0; j <array1.length ; j++) {
            System.out.println(array1[j]);
        }
    }
}


