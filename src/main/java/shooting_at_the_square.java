import java.util.Random;
import java.util.Scanner;

public class shooting_at_the_square {
    public static void main(String[] args) {
        System.out.println("All set. Get ready to rumble!");
        Scanner scn = new Scanner(System.in);
        //Fill the array
        int c, d;
        char[][] array = new char[6][6];
        for (int i = 1; i <= 5; i++) {
            for (int j = 1; j <= 5; j++) {
                array[i][j] = '-';
            }
        }
        //Generating random row and column
        Random rnd = new Random();
        int a = rnd.nextInt((5 - 1) + 1) + 1;
        int b = rnd.nextInt((5 - 1) + 1) + 1;

        do {
            System.out.print("Guess row: ");
            c = scn.nextInt();
            if (c > 5 || c <= 0) System.out.println("Enter only from 1 to 5 ");
            System.out.print("Guess column: ");
            d = scn.nextInt();
            if (d > 5 || d <= 0) System.out.println("Enter only from 1 to 5 ");
            array[a][b] = 'x';
            if (array[c][d] != array[a][b]) {
                array[c][d] = '*';
                for (int i = 1; i <= 5; i++) {
                    for (int j = 1; j <= 5; j++) {
                        if (array[i][j] == 'x') {
                            System.out.print("/ - ");
                        } else
                            System.out.print("/ " + array[i][j] + " ");
                    }
                    System.out.println();
                }
            } else {
                System.out.println("You have won");
                for (int i = 1; i <= 5; i++) {
                    for (int j = 1; j <= 5; j++) {
                        System.out.println("i " +i + "\n"+ "j:"+j);
                        System.out.print("/ " + array[i][j] + " ");
                    }
                    System.out.println();
                }
            }
        }
        while (array[c][d] != array[a][b]);
    }
}
